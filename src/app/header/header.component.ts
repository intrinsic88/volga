import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
public maleCount: number;
public femaleCount: number;
public allCount: number;
public filterApplied: boolean;
  constructor() { }

  @Input() lawyers: any;
  @Input() allCountVal: any;
  @Output() filterEvent = new EventEmitter<string>();
  ngOnInit() {
   this.filterApplied = false;
  }
  
  ngDoCheck() {
    
    this.maleCount = 0;
    this.femaleCount = 0;
    
    this.allCount = this.allCountVal;

    if (this.lawyers) {
      this.lawyers.forEach(element => {
        if (element.Gender === 'Male') {
          this.maleCount++;
        }
        if (element.Gender === 'Female') {
          this.femaleCount++;
        }
      });
    }
  }

maleFilter() {
  this.filterApplied = true;
  
  this.filterEvent.emit('Male');
}

femaleFilter() {
  this.filterApplied = true;  
  this.filterEvent.emit('Female');
}

allFilter() {
  this.filterApplied = false;  
  this.filterEvent.emit('all');
}
}
