import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class HomeServiceService {
savedJson: any;
  constructor(private http: Http) {
    let obj;
    this.getJSON().subscribe(data => obj = data);
  }

   public getJSON(): Observable<any> {
    return this.http.get('./assets/data.json')
                          .map((response: Response) => {
                            if (this.savedJson === null || this.savedJson === undefined) {
                            const data = response.json();
                            this.savedJson = data.person;
                            return data.person;
                          } else {
                              return this.savedJson;
                            }

                         });
}

public saveJson(data: any) {
  this.savedJson = data;
}

public getdata() {
  return this.savedJson;
}

}
