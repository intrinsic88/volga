import { Component, OnInit } from '@angular/core';
import { HomeServiceService } from '../home-service.service';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  public name: string;
  public age: Number;
  public gendre: string;
  public lawyers: any;
  public userId: any;
  public userIndex: any;
  constructor(private homeServiceService: HomeServiceService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.userId = JSON.parse(params['id']);
    });
    this.lawyers = this.homeServiceService.getdata();
    this.lawyers.forEach((element, key) => {
      if (element.id === this.userId) {
        this.userIndex = key;
          this.name = element.name;
          this.age = element.Age;
      }
    });
  }

  selectGender(gendre: string) {
    this.gendre = gendre;
  }

  saveForm() {
    this.lawyers = this.homeServiceService.getdata();
    this.lawyers[this.userIndex].name = this.name;
    this.lawyers[this.userIndex].Gender = this.gendre;
    this.lawyers[this.userIndex].Age = this.age;
    this.homeServiceService.saveJson(this.lawyers);
    this.router.navigate(['/view']);
  }

}
