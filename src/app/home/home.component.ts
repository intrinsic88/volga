import { Component, OnInit } from '@angular/core';
import { HomeServiceService } from '../home-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public lawyers: any;
  public unfiltered: any;
  public filterActive: boolean;
  public allCount: number;
  constructor(
    private homeServiceService: HomeServiceService
  ) {}

  ngOnInit() {
    this.homeServiceService.getJSON().subscribe(data => {
      this.lawyers = data;
      this.unfiltered = this.lawyers;
      this.allCount = this.unfiltered.length;
      this.filterActive = false;
  });
  }
filterData($event) {
  this.lawyers = [];
switch ($event) {
  case 'all':
  this.filterActive = false;
  this.lawyers = this.unfiltered;
  break;
  case 'Male':
  this.filterActive = true;
this.lawyers = this.unfiltered.filter(val => val.Gender === 'Male');
  break;
  case 'Female':
  this.filterActive = true;
  this.lawyers = this.unfiltered.filter(val => val.Gender === 'Female');
  break;

}
}
deleteArray(index: number) {
  if (this.filterActive) {
    this.lawyers.forEach((element, key) => {
      if (element.id === index) {
        this.lawyers.splice( key, 1);
    }
    });
    this.unfiltered.forEach((element, key) => {
      if (element.id === index) {
        this.unfiltered.splice( key, 1);
    }
    });
    this.allCount = this.unfiltered.length;
  } else {
    this.unfiltered.forEach((element, key) => {
      if (element.id === index) {
        this.unfiltered.splice( key, 1);
    }
    });
    this.lawyers = this.unfiltered;
    this.allCount = this.unfiltered.length;
  }
  this.homeServiceService.saveJson(this.unfiltered);
}
}
