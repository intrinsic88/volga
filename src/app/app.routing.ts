import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { HomeComponent } from './home/home.component';
import { DetailsComponent} from './details/details.component';
import { AppComponent } from './app.component';
import { AddPersonComponent } from './add-person/add-person.component';

export const AppRoutes: Routes = [
    { path: '', redirectTo: 'view', pathMatch: 'full' },
    { path: 'view', component: HomeComponent },
    { path: 'details/:id', component: DetailsComponent },
    { path: 'home', component: AppComponent},
    { path: 'add', component: AddPersonComponent}
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);