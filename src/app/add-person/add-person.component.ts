import { Component, OnInit } from '@angular/core';
import { HomeServiceService } from '../home-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.css']
})
export class AddPersonComponent implements OnInit {

  public name: string;
  public age: Number;
  public gendre: string;
  public lawyers: any;
  constructor(private homeServiceService: HomeServiceService, private router: Router) { }

  ngOnInit() {
  }

  selectGender(gendre: string) {
    this.gendre = gendre;
  }

  saveForm() {
    this.lawyers = this.homeServiceService.getdata();
    this.lawyers.push({'name': this.name, 'Gender': this.gendre, 'Age': this.age, 'id': this.lawyers.length});
    this.homeServiceService.saveJson(this.lawyers);
    this.router.navigate(['/view']);
  }

}
